# Ejemplo de markdown

Este es un texto de ejemplo


## Algunas cosas simples

Aquí voy a poner algo más

### Título de tercer nivel

Alguna cosa más 

Tomo apuntes 
y puedo poner       texto en *itálica* o en **negrita**

Puedo hacer listas sin numerar:

- uno
- dos
- tres

O listas numeradas:

1. uno
1. dos
1. tres
